/*--------------This code is for a Wemos D1 board equipped with a sensor on A0 a mosfet output on D5 and a servo output on D6
  compiled with ESP8266 v2.4.1 and OSC 1.3.3
*/

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <OSCMessage.h>
#include <Servo.h>

#define THIS_HOSTNAME "B"
#define MAXRATE 10// average delay between two status messages (in ms)
#define OTA_TIMEOUT 10 // time in seconds after which the device resume it's normal activity if no OTA firmware is comming 
//#define SERIAL_DEBUG
#define MAXWIFIATTEMPTS 120
#define MAXVALUE 1000 // maximum value to be received via OSC. This value will be mapped to the max value of the pwm output and mosfet output (1024)
#define MAXSERVOVALUE 1000 // same thing for the servo. Will be mapped to 180
#define PWMRANGE 1023

const String MACaddress = WiFi.macAddress();
static char* PSK = "malinette";
static char* AP_SSID = "malinette";
const String hostname = THIS_HOSTNAME;
const int listenPort = 9000;
int targetPort = 8000;
IPAddress broadcastIP = IPAddress({192, 168, 1, 255});
WiFiUDP udpserver;
char incomingPacket[255];
char incomingAddress[128];
long loopTimer = 0;
const String OSCprefix = "/" + hostname;
bool OTA_asked = false; // flag which become true for OTA_TIMEOUT seconds after receiving a /beginOTA message to suspend device activity while flashing
char* outputPinMode = "PWM";
Servo servoOutput;

#ifdef SERIAL_DEBUG
#define debugPrint(x)  Serial.print (x)
#define debugPrintln(x)  Serial.println (x)
#else
#define debugPrint(x)
#define debugPrintln(x)
#endif

const unsigned int outputPin = D6;
const unsigned int PWMPin = D5;
unsigned int currentReading;
unsigned int lastValueSent;

void setup() {
  WiFi.disconnect();
  char hostnameAsChar[hostname.length() + 1];
  hostname.toCharArray(hostnameAsChar, hostname.length() + 1);
#ifdef SERIAL_DEBUG
  Serial.begin(115200);
#endif
  //---------------- Hardware-specific setup ------------------
  pinMode(PWMPin, OUTPUT);
  pinMode(outputPin, OUTPUT);
  pinMode(BUILTIN_LED, OUTPUT);bool HPMode = false; // HP mode will use the mosfet to drive an HP instead of a led

  digitalWrite(BUILTIN_LED, HIGH); // the onboard LED sinks current therefore lights up when LOW
  //--------------------------------------------------------------
  connectToWifi(hostnameAsChar, AP_SSID, PSK);
  udpserver.begin(listenPort); // start listening to specified port
  loopTimer = millis();
  debugPrint("hostname : "); debugPrintln(hostname);
  delay(10);
}

void loop() {
  char hostnameAsChar[hostname.length() + 1];
  hostname.toCharArray(hostnameAsChar, hostname.length() + 1);
  if (OTA_asked) { // when receiving a /beginOTA message, this loop will wait for OTA to begin instead of carrying on the main loop
    for (int i = 0; i < OTA_TIMEOUT; i++) {
      ESP.wdtFeed();
      yield();
      ArduinoOTA.handle();
      delay(1000);
    }
    OTA_asked = false;
  }
  else {

    //---------------- Hardware-specific mainloop ------------------
    currentReading = analogRead(A0);
    if (millis() > loopTimer + MAXRATE) { // time to send data : calculate the average
      int constrainedCurrentValue = constrain(map(currentReading, 0,1023,0,1000), 0, 1023);// constrain between 0 and 1000 using the calibration
      if (constrainedCurrentValue != lastValueSent) { // avoid sending unnecessary updates
        lastValueSent = constrainedCurrentValue;
        sendValue(constrainedCurrentValue);
        loopTimer = millis();
        //debugPrint("raw data : "); debugPrintln(currentReading);
        //debugPrint("last value sent : "); debugPrintln(lastValueSent);
      }
    }
    delay(5);// needed to be able to receive OSC messages

    // Read OSC messages sent to this adress (or broadcasted)
    OSCMessage* msg = getOscMessage();
    if (msg != NULL) {
      // preconstruct char* containing this device prefix (ex: /A/something) to be matched to received adresses
      const String OSCpwmStr = OSCprefix + "/pwm";
      const char* OSCpwmAddress = OSCpwmStr.c_str();
      const String OSCservoStr = OSCprefix + "/servo";
      const char* OSCservoAddress = OSCservoStr.c_str();
      const String OSCtoneStr = OSCprefix + "/tone";
      const char* OSCtoneAddress = OSCtoneStr.c_str();
      const String OSCpowerAddressStr = OSCprefix + "/power";
      const char* OSCpowerAddress = OSCpowerAddressStr.c_str();
      const String OSCOTAStr = OSCprefix + "/OTA";
      const char* OSCOTA = OSCOTAStr.c_str();

      // ----------- PWM MODE -----------------
      //if (msg->fullMatch(OSCpwmAddress) && msg->isInt(0)) {
      if (msg->fullMatch(OSCpwmAddress) && msg->isFloat(0)) {
        //int PWMvalue = msg->getInt(0);
        float PWMvalue = msg->getFloat(0);
        if (outputPinMode != "PWM") {
          if (outputPinMode == "servo") {
            servoOutput.detach();
            debugPrint("de-activated servo output, ");
          }
          else debugPrint("de-activated tone output, ");
          analogWriteFreq(1000); // restores default PWM frequency
          outputPinMode = "PWM";
          pinMode(outputPin, OUTPUT);
          debugPrintln("activating PWM output");
        }
        debugPrint("got PWM message : "); debugPrintln(PWMvalue);
        //PWMvalue = constrain( map(PWMvalue, 0, MAXVALUE, 0, 1023), 0, 1023);
        PWMvalue = (int) mapf(PWMvalue, 0.0f, (float) MAXVALUE, 0, 1023);
        analogWrite(outputPin, PWMvalue);
        
      // ----------- SERVO MODE -----------------  
      //} else if (msg->fullMatch(OSCservoAddress)  && msg->isInt(0)) {
      } else if (msg->fullMatch(OSCservoAddress)  && msg->isFloat(0)) {
        //int servoValue = msg->getInt(0);
        float servoValue = msg->getFloat(0);
        if (outputPinMode != "servo") {
          outputPinMode = "servo";
          servoOutput.attach(outputPin);
          debugPrint("activated servo output on pin "); debugPrintln(outputPin);
          }
        debugPrint("got servo message : "); debugPrintln(servoValue);
        //servoOutput.write(constrain(map(servoValue, 0,MAXSERVOVALUE, 0, 180), 0, 180));
        servoOutput.write((int) mapf(servoValue, 0.0f ,MAXSERVOVALUE, 0, 180));

        // ----------- TONE MODE -----------------  
      //} else if (msg->fullMatch(OSCtoneAddress)  && msg->isInt(0)) {// un argument = volume max (512) , deux arguments = volume réglable
      } else if (msg->fullMatch(OSCtoneAddress)  && msg->isFloat(0)) {// un argument = volume max (512) , deux arguments = volume réglable
        if (outputPinMode != "tone") {
          if (outputPinMode == "servo") {
            servoOutput.detach();
            pinMode(outputPin, OUTPUT);
            debugPrint("de-activated servo mode, ");
          }
          else debugPrint("de-activated PWM mode, ");
          outputPinMode = "tone";
          debugPrintln("activated tone output on both outputs ");
          }
        //int toneFrequency = constrain(msg->getInt(0), 0,40000);
        float toneFrequency = (msg->getFloat(0) > 40000) ? 40000 : (int) msg->getFloat(0); // not too sure about using contraint() with floats
        analogWriteFreq(toneFrequency);
        debugPrint("got tone message : "); debugPrint(toneFrequency); debugPrint("Hz");
        //if (msg->isInt(1)) {
        if (msg->isFloat(1)) {
          //int toneVolume = constrain(map(msg->getInt(1),0,MAXVALUE,0,500), 0, 512);
          int toneVolume = (int) mapf(msg->getFloat(1),0.0f ,MAXVALUE,0.0f,512.0f);
          debugPrint(" at "); debugPrintln(toneVolume);
          analogWrite(outputPin, toneVolume);
          analogWrite(PWMPin, toneVolume);
        }
        else {
          debugPrintln(" at max volume");
          analogWrite(PWMPin, 512);
        }
        
      // ----------- MOSFET OUTPUT -----------------  
      //} else if (msg->fullMatch(OSCpowerAddress)  && msg->isInt(0)) {
      } else if (msg->fullMatch(OSCpowerAddress)  && msg->isFloat(0)) {
        //int PWMvalue = msg -> getInt(0);
        float PWMvalue = msg -> getFloat(0);
        if (outputPinMode == "tone" || outputPinMode == "servo") analogWriteFreq(1000); // reset the PWM freq to avoid frying the LED
        debugPrint("got power message : ");debugPrintln(PWMvalue);
        //PWMvalue = constrain(map(PWMvalue, 0, MAXVALUE, 0, 1023), 0, 1023);
        PWMvalue = (int) map(PWMvalue, 0.0f, MAXVALUE, 0.0f, 1023.0f);
        analogWrite(PWMPin,PWMvalue);

      // ------------- OTA ---------------
      } else if ( msg->fullMatch(OSCOTA) ) {
        debugPrintln("Asked to prepare for OTA flashing");
        OTA_asked = true; // will pause the mainloop for OTA_TIMEOUT seconds, allowing the bootloader to switch mode

      // -------- UNKNOWN MESSAGE ---------
      } else {
         debugPrint("received undefined OSC message : ");
         char addressBuffer[255];
         msg->getAddress(addressBuffer);
         debugPrintln(addressBuffer);
      }
      delete msg;
    }
    ArduinoOTA.handle();
    ESP.wdtFeed(); // avoid triggering the ESP watchdog
    yield(); // same but different
    delay(2);
  }
}

// send the OSC message "/[hostname]/input [value]" to the broadcast IP
void sendValue(int value) {
  debugPrint("Sent value "); debugPrintln(value);
  char hostnameAsChar[hostname.length() + 1];
  hostname.toCharArray(hostnameAsChar, hostname.length() + 1);
  const String OSCaddress = OSCprefix + "/input";
  OSCMessage* msg = new OSCMessage(OSCaddress.c_str());
  msg->add((int) value);
  udpserver.beginPacket(broadcastIP, targetPort);
  msg->send(udpserver);
  udpserver.endPacket();
  ESP.wdtFeed();
  yield();
  delete(msg);
}

// the OSC server itself
OSCMessage* getOscMessage() {
  int packetSize = udpserver.parsePacket();
  if (packetSize)
  {
    //Serial.printf("Received %d bytes from %s, port %d\n", packetSize, udpserver.remoteIP().toString().c_str(), udpserver.remotePort());
    int len = udpserver.read(incomingPacket, packetSize);
    if (len > 0)  {
      incomingPacket[len] = 0;
    }
    //IPfromLastMessageReceived = udpserver.remoteIP();
    OSCMessage* msg = new OSCMessage();
    msg->fill((uint8_t*)incomingPacket, len);
    int size = msg->getAddress(incomingAddress);
    ESP.wdtFeed();
    yield();
    return msg;
  }
  return NULL;
}

void connectToWifi(const char *Hostname, const char* ssid, const char* passphrase) {
  unsigned int wifiAttempts = 1;
  for (wifiAttempts; wifiAttempts < MAXWIFIATTEMPTS; wifiAttempts++ ) {
    debugPrintln("Connecting to " + String(ssid) + " : attempt #" + String(wifiAttempts));
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passphrase);
    WiFi.hostname(Hostname);
    ESP.wdtFeed();
    yield();
    if ( WiFi.waitForConnectResult() == WL_CONNECTED ) {
      debugPrintln("connected to "+ String(ssid));
      debugPrint("IP : ");debugPrintln(WiFi.localIP());
      digitalWrite(BUILTIN_LED, LOW);
      ArduinoOTA.setPort(8266); //default OTA port
      ArduinoOTA.setHostname(THIS_HOSTNAME);// No authentication by default, can be set with : ArduinoOTA.setPassword((const char *)"passphrase");
      ArduinoOTA.begin();
      break;
      }
    else {debugPrintln("failed to connect, retrying...");}
  }
  if (wifiAttempts >= MAXWIFIATTEMPTS) {
    debugPrintln("unable to connect using SSID/PSK after " + String(wifiAttempts) + " attempts, restarting...");
    for (int i=0; i<20; i++) {
      digitalWrite(BUILTIN_LED, LOW);
      delay(150);
      digitalWrite(BUILTIN_LED, HIGH);
      delay(150);
      ESP.restart();
    }
  }
}

// needed to return a float
float mapf(float value, float minIn, float maxIn, float minOut, float maxOut) {
  if (value >= maxIn) return maxOut;
  if (value <= minIn) return minOut;
  return (value-minIn) * (maxOut-minOut) / (maxIn-minIn) + minOut;
}
